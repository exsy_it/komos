var cookiesPopup = $('.cookies-popup'),
		searchPopup = $('.search-popup');

$(document).ready(function() {
	$('.main-slider').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		autoplay: true,
		customPaging: function(slider, i) {
      var thumb = $(slider.$slides[i]).data();
      return '<span></span>';
    }
	});

	$('.internship__slider').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: true,
		dots: false
	});

	// $('.sidebar__list li a').click(function (e) {
	// 	e.preventDefault();
	// 	var top = $(this).offset().top - 5;
	//
	// 	$('.sidebar__dropdown-arrow').css('top', top);
	// 	$('.sidebar__list li a').removeClass('active');
	// 	$(this).addClass('active');
	// 	$('.sidebar__dropdown').addClass('show');
	// });

	$('.sidebar__list li a').hover(function () {
		var top = $(this).offset().top - 5;

		$('.sidebar__dropdown-arrow').css('top', top);
	});


	setTimeout(function(){
		$('.cookies-popup').addClass('show');
	}, 2000);

	$('.cookies-popup__close').click(function(e) {
		e.preventDefault();
		$('.cookies-popup').removeClass('show');
	});

	$('.sidebar__burger').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$('.sidebar__wrapper').slideToggle('show');
	});

	$('.sidebar__list-arrow').click(function(e) {
		e.preventDefault();

		$(this).parent().siblings('li').find('.sidebar__list-arrow').removeClass('active');
		$(this).parent().siblings('li').find('.sidebar__dropdown').slideUp();

		$(this).toggleClass('active');
		$(this).siblings('.sidebar__dropdown').slideToggle();
	});

	$('.content__arrow').click(function(e) {
		e.preventDefault();
		var top = $($(this).attr("href")).offset().top - 60;
		$("html, body").animate({ scrollTop: top }, 500);
	});

	$('.sidebar__search').click(function(e) {
		e.preventDefault();
		searchPopup.addClass('show');
	});

	$('.search-popup__close').click(function(e) {
		e.preventDefault();
		searchPopup.removeClass('show');
	});
});
